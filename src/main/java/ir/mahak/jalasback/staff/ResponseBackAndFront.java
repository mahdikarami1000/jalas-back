/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.staff;

public class ResponseBackAndFront {

    private String message;
    private String status;
    private Long loginUserId;

    public ResponseBackAndFront(String message, String status, Long loginUserId) {
        this.message = message;
        this.status = status;
        this.loginUserId = loginUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getLoginUserId() {
        return loginUserId;
    }

    public void setLoginUserId(Long loginUserId) {
        this.loginUserId = loginUserId;
    }
}
