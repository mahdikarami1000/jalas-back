/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.domain;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Commnet")
public class Comment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Long id;
    @Expose
    private String content;
    @ManyToOne
    @Expose
    private Employee employee;
    @OneToMany(mappedBy = "fatherComment", cascade = {CascadeType.ALL, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    @Expose
    private List<Comment> replies;
    @ManyToOne
    private Comment fatherComment;
    @ManyToOne
    private JalasSession jalasSession;

    public Comment() {
    }

    public Comment(String content, Employee employee) {
        this.content = content;
        this.employee = employee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<Comment> getReplies() {
        return replies;
    }

    public void setReplies(List<Comment> replies) {
        this.replies = replies;
    }

    public void addReply(Comment reply) {
        if (this.replies == null) {
            this.replies = new ArrayList<>();
        }
        this.replies.add(reply);
    }

    public Comment getFatherComment() {
        return fatherComment;
    }

    public void setFatherComment(Comment fatherComment) {
        this.fatherComment = fatherComment;
    }

    public JalasSession getJalasSession() {
        return jalasSession;
    }

    public void setJalasSession(JalasSession jalasSession) {
        this.jalasSession = jalasSession;
    }

    public Comment findRootComment() {
        if (this.fatherComment == null) {
            return this;
        }
        Comment currentComment = this.fatherComment;
        while (currentComment.fatherComment != null) {
            currentComment = currentComment.fatherComment;
        }
        return currentComment;
    }
}
