package ir.mahak.jalasback.domain;

import com.google.gson.annotations.Expose;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "JalasSession")
public class JalasSession implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Long id;
    @Expose
    private String name;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL) // not be should eager
    @Expose
    private List<JalasDate> suggestDates;
    @Expose
    private Integer roomReserved;
    @Expose
    private String status;
    @OneToOne
    @Expose
    private JalasDate choosenDate;
    @ManyToOne
    @Expose
    private Employee reserver;
    @ManyToMany(cascade = CascadeType.ALL)
    @Expose
    private List<Employee> participants;
    @Expose
    @OneToMany(mappedBy = "jalasSession", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Comment> comments;
    @Expose
    private Long createTime;
    @Transient
    @Expose
    private List<Long> selectedEmployees;

    public JalasSession() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRoomReserved() {
        return roomReserved;
    }

    public void setRoomReserved(Integer roomReserved) {
        this.roomReserved = roomReserved;
    }

    public JalasDate getChoosenDate() {
        return choosenDate;
    }

    public void setChoosenDate(JalasDate choosenDate) {
        this.choosenDate = choosenDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Employee getReserver() {
        return reserver;
    }

    public void setReserver(Employee reserver) {
        this.reserver = reserver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<JalasDate> getSuggestDates() {
        return suggestDates;
    }

    public void setSuggestDates(List<JalasDate> suggestDates) {
        this.suggestDates = suggestDates;
    }

    public List<Employee> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Employee> participants) {
        this.participants = participants;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        if (comments == null) {
            comments = new ArrayList<>();
        }
        this.comments.add(comment);
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public List<Long> getSelectedEmployees() {
        return selectedEmployees;
    }

    public void setSelectedEmployees(List<Long> selectedEmployees) {
        this.selectedEmployees = selectedEmployees;
    }

    public void cancel() {
        this.setChoosenDate(null);
        this.setRoomReserved(null);
        this.setReserver(null);
        this.setStatus("cancel");
    }
}
