package ir.mahak.jalasback.dao;

import ir.mahak.jalasback.domain.JalasSession;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface JalasSessionDAO extends JpaRepository<JalasSession, Long> {

    public JalasSession findById(Long id);

    public JalasSession findByName(String name);

    public List<JalasSession> findByStatusAndRoomReservedIsNotNull(String status);

    public JalasSession findBySuggestDatesId(Long id);

    public JalasSession findByCommentsId(Long id);

    @Query(value = "SELECT count(*) from JalasSession WHERE status = 'cancel' or status = 'change'")
    public Long countJalasSessionCanceledOrChanged();

    @Query(value = "SELECT count(*) from JalasSession WHERE roomReserved is not null")
    public Long countRoomsReserved();

    @Query(value = "SELECT avg(createTime) from JalasSession")
    public Double avrageOfCreateASession();
}
