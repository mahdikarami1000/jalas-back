/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.staff;


public class NotificationType {
    public static String ARRANGE_MEETING
            = "arrangeMeeting";
    public static String CREATE_SESSION
            = "createSession";
    public static String ADD_TIME
            = "addTime";
    public static String ADD_PARTICIPANT
            = "addParticipant";
    public static String REMOVE_PARTICIPANT
            = "removeParticipant";
    public static String REMOVE_TIME
            = "removeTime";
    public static String NEW_VOTE
            = "newVote";
    public static String CLOSE_POLL
            = "closePoll";
}
