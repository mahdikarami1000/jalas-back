package ir.mahak.jalasback.manager;

import ir.mahak.jalasback.dao.AgreeDAO;
import ir.mahak.jalasback.dao.EmployeeDAO;
import ir.mahak.jalasback.dao.JalasSessionDAO;
import ir.mahak.jalasback.domain.Agree;
import ir.mahak.jalasback.domain.Employee;
import ir.mahak.jalasback.domain.JalasDate;
import ir.mahak.jalasback.domain.JalasSession;
import ir.mahak.jalasback.filter.TransactionFilter;
import ir.mahak.jalasback.staff.NotificationType;
import ir.mahak.jalasback.staff.ResponseBackAndFront;
import org.glassfish.jersey.client.ClientConfig;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JalasSessionManager {

    private Logger logger = Logger.getLogger(this.getClass());
    @Autowired
    JalasSessionDAO jalasSessionDAO;
    @Autowired
    NotificationManager notificationManager;
    @Autowired
    EmployeeDAO employeeDAO;
    @Autowired
    ClientConfig clientConfig;
    @Autowired
    ServiceRoutes serviceRoutes;
    @Autowired
    AgreeDAO agreeDAO;

    public ResponseEntity<?> createSession(List<Long> participantsIds, String sessionId, String title, List<JalasDate> suggestDates, Long currentUserId) {
        try {
            List<Employee> participants = employeeDAO.findByIdIn(participantsIds);
            if (sessionId.equals("-1")) {
                JalasSession jalasSession = new JalasSession();
                jalasSession.setName(title);
                jalasSession.setParticipants(participants);
                jalasSession.setStatus("notFinish");
                jalasSession.setSuggestDates(suggestDates);
                Employee reserver = employeeDAO.findById(currentUserId);
                jalasSession.setReserver(reserver);
                Long startTimeCreateSession = TransactionFilter.startTimeCreateSession;
                jalasSession.setCreateTime(System.currentTimeMillis() - startTimeCreateSession);
                jalasSessionDAO.save(jalasSession);
                jalasSession = jalasSessionDAO.findByName(title);
                for (int i = 0; i < participants.size(); i++) {
                    notificationManager.sendEmail("http://localhost:3000/session/vote/" + jalasSession.getId(), participants.get(i), title, NotificationType.CREATE_SESSION);
                }
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("ok", "200", currentUserId));
            } else {
                JalasSession jalasSession = jalasSessionDAO.findById(new Long(sessionId));
                if (!currentUserId.equals(jalasSession.getReserver().getId())) {
                    return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("عدم دسترسی", "403", currentUserId));
                }
                jalasSession.setName(title);
                List<Employee> oldParticipants = jalasSession.getParticipants();
                List<JalasDate> oldSuggestDates = jalasSession.getSuggestDates();
                jalasSession.setParticipants(participants);
                jalasSession.setStatus("change");
                jalasSession.setSuggestDates(suggestDates);
                for (int j = 0; j < suggestDates.size(); j++) {
                    for (int i = 0; i < participants.size(); i++) {
                        if (suggestDates.get(j).getId() == null) {
                            notificationManager.sendEmail("http://localhost:3000/session/vote/" + "\n تایم جدیدی اضافه شده است." + jalasSession.getId(), participants.get(i), title, NotificationType.ADD_TIME);
                        }
                    }
                }
                jalasSession = jalasSessionDAO.findByName(title);
                for (int i = 0; i < participants.size(); i++) {
                    if (oldParticipants.isEmpty()) {
                        notificationManager.sendEmail("http://localhost:3000/session/vote/" + jalasSession.getId() + " شما به این جلسه دعوت شدید.", participants.get(i), title, NotificationType.ADD_PARTICIPANT);
                    }
                    for (int j = 0; j < oldParticipants.size(); j++) {
                        if (oldParticipants.get(j).getId().equals(participants.get(i).getId())) {
                            break;
                        } else if (j == oldParticipants.size() - 1) {
                            notificationManager.sendEmail("http://localhost:3000/session/vote/" + jalasSession.getId() + " شما به این جلسه دعوت شدید.", participants.get(i), title, NotificationType.ADD_PARTICIPANT);
                        }
                    }
                }
                for (int i = 0; i < oldParticipants.size(); i++) {
                    if (participants.isEmpty()) {
                        notificationManager.sendEmail(jalasSession.getName() + " شما از این جلسه دعوت اخراج شدید.", oldParticipants.get(i), title, NotificationType.REMOVE_PARTICIPANT);
                    }
                    for (int j = 0; j < participants.size(); j++) {
                        if (oldParticipants.get(i).getId().equals(participants.get(j).getId())) {
                            break;
                        } else if (j == participants.size() - 1) {
                            notificationManager.sendEmail(jalasSession.getName() + " شما از این جلسه دعوت اخراج شدید.", oldParticipants.get(i), title, NotificationType.REMOVE_PARTICIPANT);
                        }
                    }
                }
                for (int i = 0; i < oldSuggestDates.size(); i++) {
                    if (suggestDates.isEmpty()) {
                        List<Agree> voters = agreeDAO.findByJalasDateId(oldSuggestDates.get(i).getId());
                        for (int k = 0; k < voters.size(); k++) {
                            notificationManager.sendEmail("زمان حذف شد" + oldSuggestDates.get(i).getStartDate() + oldSuggestDates.get(i).getEndDate(), participants.get(k), title, NotificationType.REMOVE_TIME);
                        }
                    }
                    for (int j = 0; j < suggestDates.size(); j++) {
                        if (oldSuggestDates.get(i).getId().equals(suggestDates.get(j).getId())) {
                            break;
                        } else if (j == suggestDates.size() - 1) {
                            List<Agree> voters = agreeDAO.findByJalasDateId(oldSuggestDates.get(i).getId());
                            for (int k = 0; k < voters.size(); k++) {
                                notificationManager.sendEmail("زمان حذف شد" + oldSuggestDates.get(i).getStartDate() + oldSuggestDates.get(i).getEndDate(), participants.get(k), title, NotificationType.REMOVE_TIME);
                            }
                        }
                    }
                }
                jalasSessionDAO.save(jalasSession);
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("ok", "200", currentUserId));
            }
        } catch (Exception ex) {
            Logger.getLogger(this.getClass()).error(ex, ex);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("حطای داخلی", "500", currentUserId));
        }
    }

}
