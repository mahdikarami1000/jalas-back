package ir.mahak.jalasback.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ir.mahak.jalasback.dao.EmployeeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ir.mahak.jalasback.domain.Employee;
import ir.mahak.jalasback.domain.NotificationManagement;
import ir.mahak.jalasback.staff.ResponseBackAndFront;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class SettingsController {

    @Autowired
    EmployeeDAO employeeDAO;

    @RequestMapping(value = "/changeNotificationSettings", method = RequestMethod.POST)
    public ResponseEntity<?> changeNotificationSettings(HttpServletRequest httpServletRequest, @RequestBody String body) {
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        Map<String, String> retMap = new Gson().fromJson(body, new TypeToken<HashMap<String, String>>() {
        }.getType());
        Boolean arrangeMeeting = Boolean.parseBoolean(retMap.get("arrangeMeeting"));
        Boolean createSession = Boolean.parseBoolean(retMap.get("createSession"));
        Boolean addTime = Boolean.parseBoolean(retMap.get("addTime"));
        Boolean addParticipant = Boolean.parseBoolean(retMap.get("addParticipant"));
        Boolean removeParticipant = Boolean.parseBoolean(retMap.get("removeParticipant"));
        Boolean removeTime = Boolean.parseBoolean(retMap.get("removeTime"));
        Boolean newVote = Boolean.parseBoolean(retMap.get("newVote"));
        Boolean closePoll = Boolean.parseBoolean(retMap.get("closePoll"));
        Employee employee = employeeDAO.findById(currentUserId);
        NotificationManagement notificationManagement = new NotificationManagement(arrangeMeeting, createSession, addTime, addParticipant, removeParticipant, removeTime, newVote, closePoll);
        employee.setNotificationManagement(notificationManagement);
        employeeDAO.save(employee);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("OK", "200", currentUserId));
    }

    @RequestMapping(value = "/getNotificationSettings", method = RequestMethod.GET)
    public ResponseEntity<?> getNotificationSettings(HttpServletRequest httpServletRequest) {
        Gson gson = new Gson();
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        Employee employee = employeeDAO.findById(currentUserId);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(gson.toJson(employee.getNotificationManagement()), "200", currentUserId));
    }
}
