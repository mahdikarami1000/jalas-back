/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.domain;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author karam
 */
@Entity
@Table(name = "NotificationManagement")
public class NotificationManagement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Long id;
    @Expose
    private Boolean arrangeMeeting;
    @Expose
    private Boolean createSession;
    @Expose
    private Boolean addTime;
    @Expose
    private Boolean addParticipant;
    @Expose
    private Boolean removeParticipant;
    @Expose
    private Boolean removeTime;
    @Expose
    private Boolean newVote;
    @Expose
    private Boolean closePoll;

    public NotificationManagement(boolean arrangeMeeting, boolean createSession, boolean addTime, boolean addParticipant, boolean removeParticipant, boolean removeTime, boolean newVote, boolean closePoll) {
        this.arrangeMeeting = arrangeMeeting;
        this.createSession = createSession;
        this.addTime = addTime;
        this.addParticipant = addParticipant;
        this.removeParticipant = removeParticipant;
        this.removeTime = removeTime;
        this.newVote = newVote;
        this.closePoll = closePoll;
    }

    public NotificationManagement() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isArrangeMeeting() {
        return arrangeMeeting;
    }

    public void setArrangeMeeting(boolean arrangeMeeting) {
        this.arrangeMeeting = arrangeMeeting;
    }

    public boolean isCreateSession() {
        return createSession;
    }

    public void setCreateSession(boolean createSession) {
        this.createSession = createSession;
    }

    public boolean isAddTime() {
        return addTime;
    }

    public void setAddTime(boolean addTime) {
        this.addTime = addTime;
    }

    public boolean isAddParticipant() {
        return addParticipant;
    }

    public void setAddParticipant(boolean addParticipant) {
        this.addParticipant = addParticipant;
    }

    public boolean isRemoveParticipant() {
        return removeParticipant;
    }

    public void setRemoveParticipant(boolean removeParticipant) {
        this.removeParticipant = removeParticipant;
    }

    public boolean isRemoveTime() {
        return removeTime;
    }

    public void setRemoveTime(boolean removeTime) {
        this.removeTime = removeTime;
    }

    public boolean isNewVote() {
        return newVote;
    }

    public void setNewVote(boolean newVote) {
        this.newVote = newVote;
    }

    public boolean isClosePoll() {
        return closePoll;
    }

    public void setClosePoll(boolean closePoll) {
        this.closePoll = closePoll;
    }

}
