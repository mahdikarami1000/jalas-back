/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.dao;

import ir.mahak.jalasback.domain.JalasDate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JalasDateDAO extends JpaRepository<JalasDate, Long> {

    public JalasDate findById(Long id);

}
