package ir.mahak.jalasback.manager;

import ir.mahak.jalasback.domain.JalasDate;

public class ServiceRoutes {
    public String reserveRoom(String roomId) {
        return "http://reserve.utse.ir/rooms/" + roomId + "/reserve";
    }

    public String jalasSession(Long jalasId) {
        return "http://localhost:3000/session/submit/" + jalasId;
    }

    public String voteRoute(Long jalasId) {
        return "http://localhost:3000/session/vote/" + jalasId;
    }

    public String availableRooms(JalasDate jalasDate) {
        return "http://reserve.utse.ir/available_rooms" + "?start=" + jalasDate.getStartDateAsSimpleString() + "&end=" + jalasDate.getEndDateAsSimpleString();
    }
}