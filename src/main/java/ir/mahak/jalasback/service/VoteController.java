package ir.mahak.jalasback.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ir.mahak.jalasback.dao.AgreeDAO;
import ir.mahak.jalasback.dao.EmployeeDAO;
import ir.mahak.jalasback.dao.JalasDateDAO;
import ir.mahak.jalasback.dao.JalasSessionDAO;
import ir.mahak.jalasback.domain.Agree;
import ir.mahak.jalasback.domain.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ir.mahak.jalasback.domain.Employee;
import ir.mahak.jalasback.domain.JalasDate;
import ir.mahak.jalasback.domain.JalasSession;
import ir.mahak.jalasback.manager.NotificationManager;
import ir.mahak.jalasback.staff.NotificationType;
import ir.mahak.jalasback.staff.ResponseBackAndFront;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class VoteController {

    @Autowired
    JalasDateDAO jalasDateDAO;

    @Autowired
    JalasSessionDAO jalasSessionDAO;

    @Autowired
    EmployeeDAO employeeDAO;

    @Autowired
    AgreeDAO agreeDAO;

    @Autowired
    NotificationManager notificationManager;

    @RequestMapping(value = "/vote", method = RequestMethod.POST)
    public ResponseEntity<?> vote(HttpServletRequest httpServletRequest, @RequestBody String body) {
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        Map<String, String> retMap = new Gson().fromJson(body, new TypeToken<HashMap<String, String>>() {
        }.getType());
        JalasDate jalasDate = jalasDateDAO.findById(new Long(retMap.get("jalasDateId")));
        JalasSession jalasSession = jalasSessionDAO.findBySuggestDatesId(new Long(retMap.get("jalasDateId")));
        Employee employee = null;
        for (int i = 0; i < jalasSession.getParticipants().size(); i++) {
            if (jalasSession.getParticipants().get(i).getId().equals(currentUserId)) {
                employee = jalasSession.getParticipants().get(i);
                break;
            } else if (i == jalasSession.getParticipants().size() - 1) {
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("عدم دسترسی برای رای دهی", "403", currentUserId));
            }
        }
        String disAgreeOrAgree = retMap.get("vote");
        Agree agree = agreeDAO.findByJalasDateIdAndVoterId(jalasDate.getId(), currentUserId);
        if (agree == null) {
            agree = new Agree();
            agree.setJalasDate(jalasDate);
            agree.setVoter(employee);
            agree.setType(disAgreeOrAgree);
        } else {
            agree.setType(disAgreeOrAgree);
        }
        agreeDAO.save(agree);
        notificationManager.sendEmail("رای جدید", jalasSession.getReserver(), "رای جدید", NotificationType.NEW_VOTE);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("OK", "200", currentUserId));
    }
}
