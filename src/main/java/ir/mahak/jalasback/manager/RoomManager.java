/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.manager;

import ir.mahak.jalasback.dao.AgreeDAO;
import ir.mahak.jalasback.dao.EmployeeDAO;
import ir.mahak.jalasback.dao.JalasSessionDAO;
import ir.mahak.jalasback.domain.Agree;
import ir.mahak.jalasback.domain.Employee;
import ir.mahak.jalasback.domain.JalasDate;
import ir.mahak.jalasback.domain.JalasSession;
import ir.mahak.jalasback.filter.TransactionFilter;
import ir.mahak.jalasback.staff.NotificationType;
import ir.mahak.jalasback.staff.ReservasionInput;
import ir.mahak.jalasback.staff.ResponseBackAndFront;

import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class RoomManager {

    private final Logger LOGGER = Logger.getLogger(this.getClass());
    @Autowired
    JalasSessionDAO jalasSessionDAO;
    @Autowired
    NotificationManager notificationManager;
    @Autowired
    EmployeeDAO employeeDAO;
    @Autowired
    AgreeDAO agreeDAO;
    ClientConfig clientConfig;
    @Autowired
    ServiceRoutes serviceRoutes;

    public ResponseEntity<?> reserveRoom(JalasDate jalasDate, String roomId, Employee employee, JalasSession jalasSession) {
        Client client = ClientBuilder.newClient(clientConfig);
        WebTarget target = client.target(serviceRoutes.reserveRoom(roomId));
        ReservasionInput reservasionInput = new ReservasionInput(employee.getName(), jalasDate.getStartDateAsSimpleString(), jalasDate.getEndDateAsSimpleString());
        Response response;
        try {
            response = target.request().accept("application/json").post(Entity.entity(reservasionInput, MediaType.APPLICATION_JSON));
            LOGGER.debug(response.readEntity(String.class));
            int status = response.getStatus();
            switch (status) {
                case 200:
                    return handleReserveOK(response, jalasDate, jalasSession, employee, roomId);
                case 404:
                    return response(HttpStatus.BAD_REQUEST, "اتاق مورد نظر یافت نشد.");
                case 500:
                    return handleReserveError(jalasDate, roomId, employee, jalasSession);
                case 400:
                    return response(HttpStatus.BAD_REQUEST, "خطا در فرمت ورودی ها");
                default:
                    throw new Exception("response code:" + status);
            }
        } catch (Exception ex) {
            jalasSession.setReserver(employee);
            jalasSession.setChoosenDate(jalasDate);
            jalasSession.setRoomReserved(new Integer(roomId));
            jalasSession.setStatus("pending");
            jalasSessionDAO.save(jalasSession);
            Logger.getLogger(this.getClass()).error(ex, ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("خطا در برقراری ارتباط با سیستم رزرواسیون. در خواست شما ثبت شد تا در صورتی برقراری ارتباط با سیستم رزرواسیون برای آنها ارسال شود.");
        }
    }

    private ResponseEntity handleReserveOK(Response response, JalasDate jalasDate, JalasSession jalasSession, Employee employee, String roomId) {
        jalasSession.setReserver(employee);
        jalasSession.setChoosenDate(jalasDate);
        jalasSession.setRoomReserved(Integer.valueOf(roomId));
        jalasSession.setStatus("finish");
        jalasSessionDAO.save(jalasSession);
        notificationManager.sendEmail(serviceRoutes.jalasSession(jalasSession.getId()), employee, "رزرو نابی", NotificationType.ARRANGE_MEETING);
        return response(HttpStatus.OK, response.readEntity(String.class));
    }

    private ResponseEntity<String> handleReserveError(JalasDate jalasDate, String roomId, Employee employee, JalasSession jalasSession) {
        jalasSession.setReserver(employee);
        jalasSession.setChoosenDate(jalasDate);
        jalasSession.setRoomReserved(new Integer(roomId));
        jalasSession.setStatus("pending");
        jalasSessionDAO.save(jalasSession);
        return response(HttpStatus.INTERNAL_SERVER_ERROR, "خطا در برقراری ارتباط با سیستم رزرواسیون. در خواست شما ثبت شد تا در صورتی برقراری ارتباط با سیستم رزرواسیون برای آنها ارسال شود.");
    }

    public ResponseEntity<?> loadRooms(JalasDate jalasDate) {
        Client client = ClientBuilder.newClient(clientConfig);
        WebTarget target = client.target(serviceRoutes.availableRooms(jalasDate));
        Response response;
        try {
            response = target.request().accept("application/json").get();
            LOGGER.debug(response.readEntity(String.class));
            int status = response.getStatus();
            switch (status) {
                case 500:
                    return response(HttpStatus.INTERNAL_SERVER_ERROR, "سرویس رزرواسیون دچار اختلال می باشد. لطفا بعدا تلاش کنید.");
                case 400:
                    return response(HttpStatus.BAD_REQUEST, "خطا در فرمت ورودی ها");
                case 200:
                    return response(HttpStatus.OK, response.readEntity(String.class));
                default:
                    throw new Exception("response cose:" + status);
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return response(HttpStatus.INTERNAL_SERVER_ERROR, "سرویس رزرواسیون دچار اختلال می باشد. لطفا بعدا تلاش کنید.");
        }
    }

    public ResponseEntity killReserve(JalasSession jalasSession) {
        if (jalasSession.getStatus().equals("finish")) {
            return response(HttpStatus.BAD_REQUEST, "متاسفانه امکان کنسل کردن رزرو موجود نمی باشد.");
        } else {
            jalasSession.cancel();
            jalasSessionDAO.save(jalasSession);
            return response(HttpStatus.OK, "ok");
        }
    }

    private ResponseEntity response(HttpStatus status, String body) {
        return ResponseEntity.status(status).body(body);
    }
}
