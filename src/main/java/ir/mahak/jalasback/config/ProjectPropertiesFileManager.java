/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.config;

import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author karam
 */
public class ProjectPropertiesFileManager {

    private static ProjectPropertiesFileManager instance;
    private Properties projectProperties;
    private String passwordEmail;
    private String usernameEmail;
    private String email;

    private Properties getProjectProperties() {
        if (projectProperties == null) {
            try {
                projectProperties = new Properties();
                projectProperties.load(this.getClass().getResourceAsStream("/application.properties"));
            } catch (IOException ex) {
                org.apache.log4j.Logger.getLogger(this.getClass()).fatal(ex, ex);
            }
        }
        return projectProperties;
    }

    private ProjectPropertiesFileManager() {

    }

    public static ProjectPropertiesFileManager getInstance() {
        if (instance == null) {
            instance = new ProjectPropertiesFileManager();
        }
        return instance;
    }

    public String getPasswordEmail() {
        if(passwordEmail == null) {
            passwordEmail = getProjectProperties().getProperty("mail.passowrd");
        }
        return passwordEmail;
    }

    public String getUsernameEmail() {
        if(usernameEmail == null) {
            usernameEmail = getProjectProperties().getProperty("mail.username");
        }
        return usernameEmail;
    }

    public String getEmail() {
        if(email == null) {
            email = getProjectProperties().getProperty("mail.email");
        }
        return email;
    }
    
}
