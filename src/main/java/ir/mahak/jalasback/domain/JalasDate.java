package ir.mahak.jalasback.domain;

import com.google.gson.annotations.Expose;
import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "JalasDate")
public class JalasDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    @Expose
    private Date startDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Expose
    private Date endDate;
    @Transient
    @Expose
    private Long agree;
    @Transient
    @Expose
    private Long disAgree;
    @Transient
    @Expose
    private Long neutralAgree;

    public JalasDate() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAgree() {
        return agree;
    }

    public void setAgree(Long agree) {
        this.agree = agree;
    }

    public Long getDisAgree() {
        return disAgree;
    }

    public void setDisAgree(Long disAgree) {
        this.disAgree = disAgree;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getNeutralAgree() {
        return neutralAgree;
    }

    public void setNeutralAgree(Long neutralAgree) {
        this.neutralAgree = neutralAgree;
    }
    
    public String getEndDateAsSimpleString() {
        return new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss").format(endDate);
    }

    public String getStartDateAsSimpleString() {
        return new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss").format(startDate);
    }
}
