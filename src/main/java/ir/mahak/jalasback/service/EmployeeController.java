package ir.mahak.jalasback.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.gson.Gson;
import ir.mahak.jalasback.dao.EmployeeDAO;
import ir.mahak.jalasback.domain.Employee;
import ir.mahak.jalasback.staff.ResponseBackAndFront;
import ir.mahak.jalasback.staff.UsernameAndPasswordShareFrontBack;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

    @Autowired
    EmployeeDAO employeeDAO;

    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public String loadAllEmployees(HttpServletRequest httpServletRequest) {
        Gson gson = new Gson();
        return gson.toJson(employeeDAO.findAll());
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody String body) {
        Gson gson = new Gson();
        UsernameAndPasswordShareFrontBack usernameAndPasswordShareFrontBack = gson.fromJson(body, UsernameAndPasswordShareFrontBack.class);
        if (usernameAndPasswordShareFrontBack.getUsername().equals("admin")) {
            List<Employee> employees = employeeDAO.findByUsername("admin");
            if (employees == null || employees.isEmpty()) {
                Employee employee = new Employee();
                employee.setMail("");
                employee.setName("مدیر");
                employee.setUsername("admin");
                employee.setPassword("Superpower110");
                employeeDAO.save(employee);
            }
        }
        List<Employee> employees = employeeDAO.findByUsername(usernameAndPasswordShareFrontBack.getUsername());
        if (employees == null || employees.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("اطلاعات کاربر نادرست است.", "403", null));
        } else {
            if (employees.get(0).getPassword().equals(usernameAndPasswordShareFrontBack.getPassword())) {
                Date expiresAt = new Date(System.currentTimeMillis() + 1000 * 3600 * 24);
                Date currentDate = new Date();
                Algorithm algorithm = Algorithm.HMAC256("jalas");
                String token = JWT.create()
                        .withIssuer("jalas.com")
                        .withExpiresAt(expiresAt)
                        .withIssuedAt(currentDate)
                        .withClaim("userId", employees.get(0).getId())
                        .sign(algorithm);
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(token, "200", employees.get(0).getId()));
            } else {
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("اطلاعات کاربر نادرست است.", "403", null));
            }
        }
    }
}
