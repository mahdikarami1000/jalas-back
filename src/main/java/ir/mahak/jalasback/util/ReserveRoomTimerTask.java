/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.util;

import ir.mahak.jalasback.dao.JalasSessionDAO;
import ir.mahak.jalasback.domain.JalasSession;
import ir.mahak.jalasback.manager.RoomManager;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ReserveRoomTimerTask {

    @Autowired
    JalasSessionDAO jalasDAO;

    @Autowired
    RoomManager roomManager;

    @Scheduled(fixedRate = 300000)
    public void run() {
        Logger.getLogger(ReserveRoomTimerTask.class).info("timer task started!");
        List<JalasSession> jalasSessions = jalasDAO.findByStatusAndRoomReservedIsNotNull("pending");
        for (int i = 0; i < jalasSessions.size(); i++) {
            JalasSession jalasSession = jalasSessions.get(i);
            roomManager.reserveRoom(jalasSession.getChoosenDate(), jalasSession.getRoomReserved().toString(), jalasSession.getReserver(), jalasSession);
        }
    }
}
