/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Agree")
public class Agree implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    private Employee voter;
    @OneToOne
    private JalasDate jalasDate;
    private String type;

    public Agree() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Employee getVoter() {
        return voter;
    }

    public void setVoter(Employee voter) {
        this.voter = voter;
    }

    public JalasDate getJalasDate() {
        return jalasDate;
    }

    public void setJalasDate(JalasDate jalasDate) {
        this.jalasDate = jalasDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
