/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.manager;

import ir.mahak.jalasback.domain.Employee;
import ir.mahak.jalasback.staff.NotificationType;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import org.apache.log4j.Logger;

public class NotificationManager {

    public void sendEmail(String content, Employee employee, String subject, String typeEmail) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if ((typeEmail.equals(NotificationType.ADD_PARTICIPANT) && employee.getNotificationManagement().isAddParticipant())
                        || (typeEmail.equals(NotificationType.ADD_TIME) && employee.getNotificationManagement().isAddTime())
                        || (typeEmail.equals(NotificationType.ARRANGE_MEETING) && employee.getNotificationManagement().isArrangeMeeting())
                        || (typeEmail.equals(NotificationType.CLOSE_POLL) && employee.getNotificationManagement().isClosePoll())
                        || (typeEmail.equals(NotificationType.CREATE_SESSION) && employee.getNotificationManagement().isCreateSession())
                        || (typeEmail.equals(NotificationType.NEW_VOTE) && employee.getNotificationManagement().isNewVote())
                        || (typeEmail.equals(NotificationType.REMOVE_PARTICIPANT) && employee.getNotificationManagement().isRemoveParticipant())
                        || (typeEmail.equals(NotificationType.REMOVE_TIME) && employee.getNotificationManagement().isRemoveTime())) {

                }
                final String username = "jalasjalaszade@gmail.com";
                final String password = "123456aA!";

                Properties prop = new Properties();
                prop.put("mail.smtp.host", "smtp.gmail.com");
                prop.put("mail.smtp.port", "465");
                prop.put("mail.smtp.auth", "true");
                prop.put("mail.smtp.socketFactory.port", "465");
                prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

                Session session = Session.getInstance(prop,
                        new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

                try {

                    Message message = new MimeMessage(session);
                    message.setFrom(new InternetAddress("from@gmail.com"));
                    message.setRecipients(
                            Message.RecipientType.TO,
                            InternetAddress.parse(employee.getMail())
                    );
                    message.setSubject(subject);
                    message.setText(content);

                    Transport.send(message);
                } catch (Exception ex) {
                    Logger.getLogger(NotificationManager.class).error(ex, ex);
                }
            }
        }).start();

    }
}
