package ir.mahak.jalasback.manager;

import ir.mahak.jalasback.domain.JalasDate;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ManagerConfig {

    @Bean
    @Scope(value = "singleton")
    public ClientConfig clientConfig() {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.property(ClientProperties.CONNECT_TIMEOUT, 3000);
        clientConfig.property(ClientProperties.READ_TIMEOUT, 5000);
        return clientConfig;
    }

    @Bean
    @Scope(value = "singleton")
    public ServiceRoutes servicesRoute() {
        return new ServiceRoutes();
    }

    @Bean
    @Scope(value = "singleton")
    public NotificationManager notificationManager() {
        return new NotificationManager();
    }
}
