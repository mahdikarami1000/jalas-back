/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.dao;

import ir.mahak.jalasback.domain.Employee;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeDAO extends JpaRepository<Employee, Long> {

    public Employee findById(Long id);

    public List<Employee> findByIdIn(List<Long> ids);

    public List<Employee> findByUsername(String username);
}
