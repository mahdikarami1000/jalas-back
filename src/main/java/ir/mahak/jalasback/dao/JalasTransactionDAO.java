package ir.mahak.jalasback.dao;

import ir.mahak.jalasback.domain.JalasTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface JalasTransactionDAO extends JpaRepository<JalasTransaction, Long> {

    @Query(value = "SELECT avg(duration) from JalasTransaction")
    public Double getAverageTime();

    @Query(value = "SELECT (count(*) * 1000000000 )/ (max(endTime) - min(startTime)) from JalasTransaction")
    public Double getTps();
}
