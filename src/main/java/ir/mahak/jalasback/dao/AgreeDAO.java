/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.dao;

import ir.mahak.jalasback.domain.Agree;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgreeDAO extends JpaRepository<Agree, Long> {

    public Long countByJalasDateIdAndType(Long Id, String type);

    public Agree findByJalasDateIdAndVoterId(Long jalasDateId, Long voterId);
    
    public List<Agree> findByJalasDateId(Long jalasDateId);
}
