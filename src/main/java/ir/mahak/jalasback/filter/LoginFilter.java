/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.filter;

import ir.mahak.jalasback.exception.NotSetJwtHeaderException;
import ir.mahak.jalasback.exception.NotFoundAnyHeadersFromRequestException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.gson.Gson;
import ir.mahak.jalasback.staff.ResponseBackAndFront;
import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig fc) throws ServletException {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        Gson gson = new Gson();
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With,observe, jwtHeader");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Expose-Headers", "Authorization");
        response.addHeader("Access-Control-Expose-Headers", "responseType");
        response.addHeader("Access-Control-Expose-Headers", "observe");
        String path = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length());
        String jwtHeader = null;
        try {
            if (!path.startsWith("/login") && !path.startsWith("/signup")) {
                Enumeration<String> headerNames = httpRequest.getHeaderNames();
                if (headerNames == null) {
                    throw new NotFoundAnyHeadersFromRequestException();
                }
                while (headerNames.hasMoreElements()) {
                    String headerName = headerNames.nextElement();
                    if (headerName.equalsIgnoreCase("jwtHeader")) {
                        jwtHeader = httpRequest.getHeader(headerName);
                    }
                }
                if (jwtHeader == null || jwtHeader.equals("null")) {
                    throw new NotSetJwtHeaderException();
                }
                Algorithm algorithm = Algorithm.HMAC256("jalas");
                JWTVerifier verifier = JWT.require(algorithm)
                        .withIssuer("jalas.com")
                        .build();
                DecodedJWT jwt = verifier.verify(jwtHeader);
                Long userId = jwt.getClaim("userId").asLong();
                request.setAttribute("currentUserId", userId);
            }
            chain.doFilter(request, resp);
        } catch (NotSetJwtHeaderException ex) {
            Logger.getLogger(LoginFilter.class).error(ex, ex);
            ResponseBackAndFront responsePostMessage = new ResponseBackAndFront("شما در سامانه لاگین نکرده اید!", "401", null);
            response.setStatus(HttpServletResponse.SC_OK);
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(gson.toJson(responsePostMessage));
        } catch (NotFoundAnyHeadersFromRequestException ex) {
            Logger.getLogger(LoginFilter.class).error(ex, ex);
            ResponseBackAndFront responsePostMessage = new ResponseBackAndFront("حطا در فراخوانی عملیات", "400", null);
            response.setStatus(HttpServletResponse.SC_OK);
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(gson.toJson(responsePostMessage));
        } catch (JWTVerificationException ex) {
            Logger.getLogger(LoginFilter.class).error(ex, ex);
            ResponseBackAndFront responsePostMessage = new ResponseBackAndFront("شما دسترسی لازم را ندارید.", "403", null);
            response.setStatus(HttpServletResponse.SC_OK);
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(gson.toJson(responsePostMessage));
        }
    }

}
