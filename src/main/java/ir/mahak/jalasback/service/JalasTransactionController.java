package ir.mahak.jalasback.service;

import ir.mahak.jalasback.dao.JalasTransactionDAO;
import ir.mahak.jalasback.filter.TransactionFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ir.mahak.jalasback.staff.ResponseBackAndFront;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
public class JalasTransactionController {

    @Autowired
    JalasTransactionDAO jalasTransactionDAO;

    @RequestMapping(value = "/avgTransaction", method = RequestMethod.GET)
    public ResponseEntity<?> avgTransaction() {
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(Long.toString((TransactionFilter.transactionNumber * 1000000) / (System.currentTimeMillis() - TransactionFilter.startTime)), "200", null));
    }

    @RequestMapping(value = "/tpsTransaction", method = RequestMethod.GET)
    public ResponseEntity<?> tpsTransaction() {
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(jalasTransactionDAO.getTps().toString(), "200", null));
    }
}
