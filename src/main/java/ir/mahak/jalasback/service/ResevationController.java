package ir.mahak.jalasback.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ir.mahak.jalasback.dao.EmployeeDAO;
import ir.mahak.jalasback.dao.JalasDateDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ir.mahak.jalasback.dao.JalasSessionDAO;
import ir.mahak.jalasback.domain.Employee;
import ir.mahak.jalasback.domain.JalasDate;
import ir.mahak.jalasback.domain.JalasSession;
import ir.mahak.jalasback.manager.RoomManager;
import java.util.HashMap;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
public class ResevationController {

    @Autowired
    JalasSessionDAO jalasSessionDAO;

    @Autowired
    EmployeeDAO employeeDAO;

    @Autowired
    JalasDateDAO jalasDateDAO;

    @Autowired
    RoomManager roomManager;

    @RequestMapping(value = "/room", method = RequestMethod.GET)
    public ResponseEntity<?> loadRoomsByDates(
            @RequestParam(value = "jalasDateId") String jalasDateId) {
        JalasDate jalasDate = jalasDateDAO.findById(new Long(jalasDateId));
        return roomManager.loadRooms(jalasDate);
    }

    @RequestMapping(value = "/room", method = RequestMethod.POST)
    public ResponseEntity<?> reserveRoomWithDates(
            @RequestBody String body) {
        Map<String, String> retMap = new Gson().fromJson(body, new TypeToken<HashMap<String, String>>() {
        }.getType());
        JalasDate jalasDate = jalasDateDAO.findById(new Long(retMap.get("jalasDateId")));
        Employee employee = employeeDAO.findById(new Long(retMap.get("userId")));
        JalasSession jalasSession = jalasSessionDAO.findById(new Long(retMap.get("sessionId")));
        return roomManager.reserveRoom(jalasDate, retMap.get("roomId"), employee, jalasSession);
    }

    @RequestMapping(value = "/killReserve", method = RequestMethod.POST)
    public ResponseEntity<?> killReserve(
            @RequestBody String body) {
        Map<String, String> retMap = new Gson().fromJson(body, new TypeToken<HashMap<String, String>>() {
        }.getType());
        JalasSession jalasSession = jalasSessionDAO.findById(new Long(retMap.get("sessionId")));
        return roomManager.killReserve(jalasSession);
    }

}
