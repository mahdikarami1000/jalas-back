package ir.mahak.jalasback.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ir.mahak.jalasback.dao.CommentDAO;
import ir.mahak.jalasback.dao.EmployeeDAO;
import ir.mahak.jalasback.dao.JalasSessionDAO;
import ir.mahak.jalasback.domain.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ir.mahak.jalasback.domain.Employee;
import ir.mahak.jalasback.domain.JalasSession;
import ir.mahak.jalasback.staff.ResponseBackAndFront;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class CommentController {

    @Autowired
    JalasSessionDAO jalasSessionDAO;

    @Autowired
    EmployeeDAO employeeDAO;

    @Autowired
    CommentDAO commnetDAO;

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public ResponseEntity<?> addComment(HttpServletRequest httpServletRequest, @RequestBody String body) {
        Map<String, String> retMap = new Gson().fromJson(body, new TypeToken<HashMap<String, String>>() {
        }.getType());
        JalasSession jalasSession = jalasSessionDAO.findById(new Long(retMap.get("jalasSessionId")));
        String content = retMap.get("content");
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        Employee employee = employeeDAO.findById(currentUserId);
        Comment comment = new Comment(content, employee);
        jalasSession.addComment(comment);
        comment.setJalasSession(jalasSession);
        jalasSessionDAO.save(jalasSession);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("نظر با موفقیت به ثبت رسید.", "200", currentUserId));
    }

    @RequestMapping(value = "/replyComment", method = RequestMethod.POST)
    public ResponseEntity<?> replyComment(HttpServletRequest httpServletRequest, @RequestBody String body) {
        Map<String, String> retMap = new Gson().fromJson(body, new TypeToken<HashMap<String, String>>() {
        }.getType());
        String content = retMap.get("content");
        String mainCommentId = retMap.get("mainCommentId");
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        Employee employee = employeeDAO.findById(currentUserId);
        Comment mainComment = commnetDAO.findById(Long.parseLong(mainCommentId));
        Comment comment = new Comment(content, employee);
        comment.setFatherComment(mainComment);
        mainComment.addReply(comment);
        commnetDAO.save(mainComment);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("نظر با موفقیت به ثبت رسید.", "200", currentUserId));
    }

    @RequestMapping(value = "/deleteComment", method = RequestMethod.POST)
    public ResponseEntity<?> deleteComment(HttpServletRequest httpServletRequest, @RequestBody String body) {
        Map<String, String> retMap = new Gson().fromJson(body, new TypeToken<HashMap<String, String>>() {
        }.getType());
        String commentId = retMap.get("commentId");
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        Comment comment = commnetDAO.findById(Long.parseLong(commentId));
        Comment rootComment = comment.findRootComment();
        JalasSession jalasSession = jalasSessionDAO.findByCommentsId(rootComment.getId());
        if (comment.getEmployee().getId().equals(currentUserId) || currentUserId.equals(jalasSession.getReserver().getId())) {
            if (comment.getFatherComment() == null) {
                comment.setJalasSession(null);
                commnetDAO.save(comment);
            } else {
                comment.setFatherComment(null);
                commnetDAO.save(comment);
            }
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("نظر با موفقیت به ثبت رسید.", "200", currentUserId));
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("شما دسترسی لازم برای پاک کردن این نظر را ندارید.", "403", currentUserId));
        }

    }
}
