/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.staff;

import java.util.List;

/**
 *
 * @author karam
 */
public class CreateSessionShareFrontBack {

    private String sessionId;
    private String title;
    private List<StartEndDateShareFrontBack> suggestDates;
    private List<String> participantsIds;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<StartEndDateShareFrontBack> getSuggestDates() {
        return suggestDates;
    }

    public void setSuggestDates(List<StartEndDateShareFrontBack> suggestDates) {
        this.suggestDates = suggestDates;
    }

    public List<String> getParticipantsIds() {
        return participantsIds;
    }

    public void setParticipantsIds(List<String> participantsIds) {
        this.participantsIds = participantsIds;
    }

}
