package ir.mahak.jalasback.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ir.mahak.jalasback.dao.AgreeDAO;
import ir.mahak.jalasback.dao.EmployeeDAO;
import ir.mahak.jalasback.manager.JalasSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ir.mahak.jalasback.dao.JalasSessionDAO;
import ir.mahak.jalasback.domain.JalasDate;
import ir.mahak.jalasback.domain.JalasSession;
import ir.mahak.jalasback.manager.RoomManager;
import ir.mahak.jalasback.staff.CreateSessionShareFrontBack;
import ir.mahak.jalasback.staff.ResponseBackAndFront;
import ir.mahak.jalasback.staff.StartEndDateShareFrontBack;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class JalasSessionController {

    @Autowired
    AgreeDAO agreeDAO;

    @Autowired
    JalasSessionDAO jalasSessionDAO;

    @Autowired
    RoomManager roomManager;

    @Autowired
    JalasSessionManager jalasSessionManager;

    @Autowired
    EmployeeDAO employeeDAO;

    @RequestMapping(value = "/session/vote/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> loadSessionVoteById(HttpServletRequest httpServletRequest, @PathVariable("id") Long id) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        JalasSession jalasSession = jalasSessionDAO.findById(id);
        for (int i = 0; i < jalasSession.getParticipants().size(); i++) {
            if (jalasSession.getParticipants().get(i).getId().equals(currentUserId) || jalasSession.getReserver().getId().equals(currentUserId)) {
                break;
            } else if (i == jalasSession.getParticipants().size() - 1) {
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("عدم دسترسی", "403", currentUserId));
            }
        }
        List<JalasDate> suggestDates = jalasSession.getSuggestDates();
        for (int i = 0; i < suggestDates.size(); i++) {
            suggestDates.get(i).setAgree(agreeDAO.countByJalasDateIdAndType(suggestDates.get(i).getId(), "agree"));
            suggestDates.get(i).setNeutralAgree(agreeDAO.countByJalasDateIdAndType(suggestDates.get(i).getId(), "neutral"));
            suggestDates.get(i).setDisAgree(agreeDAO.countByJalasDateIdAndType(suggestDates.get(i).getId(), "disAgree"));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(gson.toJson(jalasSession), "200", currentUserId));
    }

    @RequestMapping(value = "/session/submit/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> loadSessionSubmitById(HttpServletRequest httpServletRequest, @PathVariable("id") Long id) {
        Gson gson = new Gson();
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        JalasSession jalasSession = jalasSessionDAO.findById(id);
        if (!jalasSession.getReserver().getId().equals(currentUserId)) {
            if (jalasSession.getStatus().equals("finish")) {
                for (int i = 0; i < jalasSession.getParticipants().size(); i++) {
                    if (jalasSession.getParticipants().get(i).getId().equals(currentUserId)) {
                        break;
                    } else if (i == jalasSession.getParticipants().size() - 1) {
                        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("عدم دسترسی", "403", currentUserId));
                    }
                }
            } else {
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("عدم دسترسی", "403", currentUserId));
            }
        }

        List<JalasDate> suggestDates = jalasSession.getSuggestDates();
        for (int i = 0; i < suggestDates.size(); i++) {
            suggestDates.get(i).setAgree(agreeDAO.countByJalasDateIdAndType(suggestDates.get(i).getId(), "agree"));
            suggestDates.get(i).setNeutralAgree(agreeDAO.countByJalasDateIdAndType(suggestDates.get(i).getId(), "neutral"));
            suggestDates.get(i).setDisAgree(agreeDAO.countByJalasDateIdAndType(suggestDates.get(i).getId(), "disAgree"));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(gson.toJson(jalasSession), "200", currentUserId));
    }

    @RequestMapping(value = "/session/create/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> loadSessionCreateById(HttpServletRequest httpServletRequest, @PathVariable("id") Long id) {
        Gson gson = new GsonBuilder().setDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS'Z'").create();
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        JalasSession jalasSession = jalasSessionDAO.findById(id);
        if (!jalasSession.getReserver().getId().equals(currentUserId)) {
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront("عدم دسترسی", "403", currentUserId));
        }

        List<Long> selectedEmployeesId = new ArrayList<>();
        for (int i = 0; i < jalasSession.getParticipants().size(); i++) {
            selectedEmployeesId.add(jalasSession.getParticipants().get(i).getId());
        }
        jalasSession.setSelectedEmployees(selectedEmployeesId);
        List<JalasDate> suggestDates = jalasSession.getSuggestDates();
        for (int i = 0; i < suggestDates.size(); i++) {
            suggestDates.get(i).setAgree(agreeDAO.countByJalasDateIdAndType(suggestDates.get(i).getId(), "agree"));
            suggestDates.get(i).setDisAgree(agreeDAO.countByJalasDateIdAndType(suggestDates.get(i).getId(), "disAgree"));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(gson.toJson(jalasSession), "200", currentUserId));
    }

    @RequestMapping(value = "/createSession", method = RequestMethod.POST)
    public ResponseEntity<?> createSession(HttpServletRequest httpServletRequest, @RequestBody String body) throws ParseException {
        Gson gson = new Gson();
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        SimpleDateFormat formatterOut = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS'Z'");
        CreateSessionShareFrontBack createSessionShareFrontBack = gson.fromJson(body, CreateSessionShareFrontBack.class);
        String sessionId = createSessionShareFrontBack.getSessionId();
        String title = createSessionShareFrontBack.getTitle();
        List<StartEndDateShareFrontBack> suggestDates = createSessionShareFrontBack.getSuggestDates();
        List<String> participantsIds = createSessionShareFrontBack.getParticipantsIds();
        List<Long> participantsIdsLong = new ArrayList<>();
        for (String s : participantsIds) {
            participantsIdsLong.add(Long.valueOf(s));
        }
        List<JalasDate> jalasDates = new ArrayList<>();
        for (StartEndDateShareFrontBack s : suggestDates) {
            JalasDate jalasDate = new JalasDate();
            String startDate = s.getStartDate();
            String endDate = s.getEndDate();
            jalasDate.setStartDate(formatterOut.parse(startDate));
            jalasDate.setEndDate(formatterOut.parse(endDate));
            jalasDate.setId(s.getId());
            jalasDates.add(jalasDate);
        }
        return jalasSessionManager.createSession(participantsIdsLong, sessionId, title, jalasDates, currentUserId);
    }

    @RequestMapping(value = "/loadMeetingsByParticipationUserId", method = RequestMethod.GET)
    public ResponseEntity<?> loadMeetingsByParticipationUserId(HttpServletRequest httpServletRequest) throws ParseException {
        Gson gson = new Gson();
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        List<JalasSession> jalasSessions = new ArrayList<>();
        List<JalasSession> allJalasSessions = jalasSessionDAO.findAll();
        for (int i = 0; i < allJalasSessions.size(); i++) {
            if (allJalasSessions.get(i).getReserver().getId().equals(currentUserId)) {
                jalasSessions.add(allJalasSessions.get(i));
            } else {
                if (allJalasSessions.get(i).getStatus().equals("finish")) {
                    for (int j = 0; j < allJalasSessions.get(i).getParticipants().size(); j++) {
                        if (allJalasSessions.get(i).getParticipants().get(j).getId().equals(currentUserId)) {
                            jalasSessions.add(allJalasSessions.get(i));
                            break;
                        }
                    }
                }
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(gson.toJson(jalasSessions), "200", currentUserId));
    }

    @RequestMapping(value = "/loadPollsByParticipationUserId", method = RequestMethod.GET)
    public ResponseEntity<?> loadPollsByParticipationUserId(HttpServletRequest httpServletRequest) throws ParseException {
        Gson gson = new Gson();
        Long currentUserId = (Long) httpServletRequest.getAttribute("currentUserId");
        List<JalasSession> jalasSessions = new ArrayList<>();
        List<JalasSession> allJalasSessions = jalasSessionDAO.findAll();
        for (int i = 0; i < allJalasSessions.size(); i++) {
            if (allJalasSessions.get(i).getReserver().getId().equals(currentUserId)) {
                jalasSessions.add(allJalasSessions.get(i));
            } else {
                for (int j = 0; j < allJalasSessions.get(i).getParticipants().size(); j++) {
                    if (allJalasSessions.get(i).getParticipants().get(j).getId().equals(currentUserId)) {
                        jalasSessions.add(allJalasSessions.get(i));
                        break;
                    }
                }
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(gson.toJson(jalasSessions), "200", currentUserId));
    }

    @RequestMapping(value = "/countJalasSessionCanceledOrChanged", method = RequestMethod.GET)
    public ResponseEntity<?> countJalasSessionCanceledOrChanged() throws ParseException {
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(jalasSessionDAO.countJalasSessionCanceledOrChanged().toString(), "200", null));
    }

    @RequestMapping(value = "/countRoomsReserved", method = RequestMethod.GET)
    public ResponseEntity<?> countRoomsReserved() throws ParseException {
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(jalasSessionDAO.countRoomsReserved().toString(), "200", null));
    }

    @RequestMapping(value = "/avrageOfCreateASession", method = RequestMethod.GET)
    public ResponseEntity<?> avrageOfCreateASession() throws ParseException {
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseBackAndFront(jalasSessionDAO.avrageOfCreateASession().toString(), "200", null));
    }

}
