/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ir.mahak.jalasback.filter;

import ir.mahak.jalasback.dao.JalasTransactionDAO;
import ir.mahak.jalasback.domain.JalasTransaction;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class TransactionFilter implements Filter {

    @Autowired
    JalasTransactionDAO jalasTransactionDAO;

    public static Long startTimeCreateSession;
    public static Long startTime;
    public static Long transactionNumber;

    @Override
    public void init(FilterConfig fc) throws ServletException {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        if (startTime == null) {
            startTime = System.currentTimeMillis();
            transactionNumber = 0L;
        }
        transactionNumber += 1;
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String path = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length());
        if (path.endsWith("employee")) {
            startTimeCreateSession = System.currentTimeMillis();
        }
        long st = System.currentTimeMillis();
       
        chain.doFilter(request, resp);
        long et = System.currentTimeMillis();
        long tt = (et - st);
        JalasTransaction jalasTransaction = new JalasTransaction(st, et, tt, path);
        jalasTransactionDAO.save(jalasTransaction);
    }

}
